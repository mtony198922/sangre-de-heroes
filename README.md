###Project General Information 

Patch 4.3.4 - NDP-DB 434.01.01 

- Level 85;
- Daily corrections;
- Great stability;
- All spells are being worked;
- Professionally developed;
- All class and races combinations are fixed.

## Introduction
projecto oficial server sangre de heroes 
www.sangredeheroes.com

## Requirements

+ Platform: Linux, Windows or Mac
+ Processor with SSE2 support
+ Boost ≥ 1.49
+ MySQL ≥ 5.1.0
+ CMake ≥ 2.8.11.2 / 2.8.9 (Windows / Linux)
+ OpenSSL ≥ 1.0.0
+ GCC ≥ 4.7.2 (Linux only)
+ MS Visual Studio ≥ 12 (2013 Update 4) (Windows only)

## Install

Detailed installation guides are available in the [TrinityCore Wiki](http://collab.kpsn.org/display/tc/Installation+Guide) for
Windows, Linux and Mac OSX.


## Copyright

License: GPL 2.0

Read file [COPYING](COPYING)


## Authors & Contributors

Read file [THANKS](THANKS)

